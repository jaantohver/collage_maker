
# Collage Maker

Code for the final project of Digital Image Processing at University of Tartu. Autumn/winter 2018.

## Dependencies

TODO written from memory. If there are any problems then fix.

* Pillow
	* `pip install Pillow`
* SciPy packages
	* follow instructions at https://scipy.org/install.html
* Google Images Download
	* `pip install google_images_download`
* OpenCV
	* use prebuilt packages
		* `pip install opencv-python`
		* `pip install opencv-contrib-python`
	* or build from source following platform specific instructions

## Run

`python collage_maker.py`