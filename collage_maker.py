from __future__ import division

import sys

# py version check
if sys.version_info[0] < 3:
    import Tkinter as tk
    from Tkinter import *
    import tkFileDialog as filedialog
    from tkColorChooser import askcolor
else:
    import tkinter as tk
    from tkinter import *
    from tkinter import filedialog
    from tkinter.colorchooser import *

from PIL import ImageTk, Image

try:
    from PIL import ImageGrab
except ImportError:
    import pyscreenshot as ImageGrab

import cv2
import numpy as np
from google_images_download import google_images_download as gid
import random

import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
import matplotlib.backends.tkagg as tkagg
from matplotlib.backends.backend_tkagg import FigureCanvasAgg


class CollageMaker(tk.Frame):

    searchBoxes = []
    canvasImages = []
    sorted_image_paths = []
    borderColour = (255, 255, 255)
    mat = np.zeros((720, 720), np.int32)
    images = []
    borderLines = []

    def Apply(self):
        global noise

        if (not hasattr(self, 'img') or self.img is None):
            return

        self.new_img = Image.eval(self.img, lambda x: x * self.var_contrast.get() + self.var_brightness.get())

        if (self.var_gs.get() == 0 and self.var_bn.get() == 0 and self.var_noise.get() == 0 and self.var_gnoise.get() == 0):
            # If no filters selected
            self.canvas.image = ImageTk.PhotoImage(self.new_img)
        elif self.var_gs.get() == 1:
            # If grayscale selected
            self.new_img = self.grayscale(self.new_img)
            self.canvas.image = ImageTk.PhotoImage(self.new_img)
        elif self.var_bn.get() == 1:
            # If no grayscale and binary selected
            self.new_img = self.binary(self.new_img)
            self.canvas.image = ImageTk.PhotoImage(self.new_img)

        # ADD SALT & PEPPER NOISE
        if self.var_noise.get() == 1 and self.var_gnoise.get() == 0:
            # If s&p selected
            self.new_img = self.spNoise(self.new_img)
            self.canvas.image = ImageTk.PhotoImage(self.new_img)

        # ADD GAUSSIAN NOISE
        elif self.var_gnoise.get() == 1:
            # If no s&p and grayscale selected
            self.new_img = self.gaussianNoise(self.new_img)
            self.canvas.image = ImageTk.PhotoImage(self.new_img)

        self.canvas.create_image(0, 0, image=self.canvas.image, anchor='nw')

    def addLabel(self):
        entry = tk.Entry(self.root)
        entry.grid(row=self.searchCounter, column=0, columnspan=2)
        entry.focus_force()

        self.searchBoxes.append(entry)

        #self.addButton.grid(row=self.searchCounter, column=1)
        self.addButton.grid(row=self.searchCounter + 1, column=0)
        self.removeButton.grid(row=self.searchCounter + 1, column=1)

        self.searchCounter += 1
        
    def removeLabel(self):
        entry = self.searchBoxes.pop()
        entry.destroy()
        
        #self.addButton.grid(row=self.searchCounter, column=1)
        self.addButton.grid(row=self.searchCounter - 1, column=0)
        self.removeButton.grid(row=self.searchCounter - 1, column=1)

        self.searchCounter -= 1

    def search(self):
        self.canvasImages = []
        self.canvas.delete("all")

        keywords = []
        maxCount = 1

        for b in self.searchBoxes:
            if len(b.get()) != 0:
                found = False
                for i, (count, value) in enumerate(keywords):
                    if value == b.get():
                        if count + 1 > maxCount:
                            maxCount = count + 1

                        keywords[i] = (count + 1, value)
                        found = True
                        pass

                if not found:
                    keywords.append((1, b.get()))

        if len(keywords) == 0:
            return

        bg = True
        for i in range(1, maxCount + 1):
            searchKeywords = [value for (count, value) in keywords if count == i]
            if len(searchKeywords) > 0:
                arguments = {
                    "keywords": ",".join(searchKeywords),
                    "limit": i,
                    "print_urls": False,
                    "format": "png"
                }

                response = gid.googleimagesdownload()
                absolute_image_paths = response.download(arguments)
                index_map = {v: i for i, v in enumerate(searchKeywords)}
                self.sorted_image_paths = sorted(absolute_image_paths.items(), key=lambda pair: index_map[pair[0]])
                self.display(self.sorted_image_paths, bg)
                bg = False

    def rearrange(self):
        self.canvasImages = []
        self.canvas.delete("all")
        self.display(self.sorted_image_paths, self.var_bg.get())
        return

    def resize(self, image, size):
        height, width = image.shape[:2]
        if height > width:
            new = int((height / width) * size)

            dim = (size, new)
        else:
            new = int((width / height) * size)
            dim = (new, size)

        image = cv2.resize(image, dim, interpolation=cv2.INTER_CUBIC)
        image = cv2.cvtColor(image, cv2.COLOR_BGRA2RGBA)

        return image, dim

    def area(self, a, b):  
        dx = min(a[1][0], b[1][0]) - max(a[0][0], b[0][0])
        dy = min(a[0][1], b[0][1]) - max(a[1][1], b[1][1])
        if (dx>=0) and (dy>=0):
            return dx*dy
        else:
            return 0

    def get_coords(self, dim, pc_l, iters):
        sums = []
        coords = []
        x = 0
        y = 0
        for i in range(iters):
            try:
                y = random.randint(0, self.height-dim[1])
                x = random.randint(0, self.width-dim[0])
                rec  = [ [x, y+dim[1]], [x + dim[0], y] ] 
                s = 0
                for j in range(len(pc_l)):
                    s += self.area(rec, pc_l[j])
                if s == 0:
                    break
                sums.append([s])
                coords.append([x, y])
                if i == iters-1:
                    x = coords[sums.index(min(sums))][0]
                    y = coords[sums.index(min(sums))][1]
            except ValueError:
                continue

        return x, y

    def display(self, imagePaths, bg):
        pc_l = []
        self.coords = []
        self.bg_enabled = False
        self.images = []
        for i in imagePaths:
            paths = i[1]
            for path in paths:
                image = cv2.imread(path)
                try:
                    height, width = image.shape[:2]
                except AttributeError:
                    continue

                if bg and self.var_bg.get() == 1:
                    x, y = 0, 0
                    image, dim = self.resize(image, self.width)
                    pilImage = Image.fromarray(image)
                    canvasImage = ImageTk.PhotoImage(pilImage)
                    self.canvasImages.append(canvasImage)
                    self.canvas.create_image(x, y, image=canvasImage, anchor='nw')
                    bg = False
                    self.bg_enabled = True
                    self.coords.append([0,0])
                    self.images.append(image)
                    break
                else:
                    size = random.randint(100, 300)
                    try:
                        image = self.extract_content(image)
                    except ValueError:
                        continue

                    image, dim = self.resize(image, size)
                    
                    x, y = self.get_coords(dim, pc_l, 1000)
                    pc_l.append( [ [x, y+dim[1]], [x+dim[0], y] ])
                    self.coords.append([x,y])

                    pilImage = Image.fromarray(image)
                    canvasImage = ImageTk.PhotoImage(pilImage)
                    self.canvasImages.append(canvasImage)
                    self.canvas.create_image(x, y, image=canvasImage, anchor='nw')

                    self.images.append(image)
    
    def refresh(self):
        self.canvasImages = []
        self.canvas.delete("all")

        for i in range(len(self.images)):
            x = self.coords[i][0]
            y = self.coords[i][1]

            pilImage = Image.fromarray(self.images[i])
            canvasImage = ImageTk.PhotoImage(pilImage)
            self.canvasImages.append(canvasImage)
            self.canvas.create_image(x, y, image=canvasImage, anchor='nw')

        self.drawBorder()


    def extract_content(self, image):
        gry = cv2.cvtColor(image.copy(), cv2.COLOR_RGB2GRAY)
        blur = cv2.GaussianBlur(gry, (5, 5), 0)
        ret, th = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        edges = cv2.Canny(gry, ret / 2, ret)
        kernel = np.ones((5, 5), np.uint8)
        dilation = cv2.dilate(edges, kernel, iterations=3)
        img = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)
        mask = np.zeros(img.shape, dtype="uint8")
        cont_im, contours, hierarchy = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        c = max(contours, key=cv2.contourArea)
        cv2.drawContours(mask, [c], 0, (255, 255, 255, 255), -1)
        maskedImg = cv2.bitwise_and(img, mask)

        return maskedImg

    def borderSelected(self, *args):
        border = self.border.get()

        if border == "None":
            self.colourBox.grid_remove()
            self.colourPickerButton.grid_remove()
            self.drawBorder()
        elif border == "Solid":
            self.colourBox.grid()
            self.colourPickerButton.grid()
            self.drawBorder()
        else:
            pass

    def getColour(self):
        colour = askcolor()
        if colour[0] and colour[1]:  # & &
            self.borderColour = colour[0]
            self.colourBox['bg'] = colour[1]
            
        self.drawBorder()
        
    def drawBorder(self, *args):
        t = self.var_thickness.get()
        colour = self.colourBox.cget('bg')

        for line in self.borderLines:
            self.canvas.delete(line)

        self.borderLines = []

        if self.border.get() == "None":
            return

        #top
        self.borderLines.append(self.canvas.create_line(0, 0, self.width, 0, width=t, fill=colour))
        #right
        self.borderLines.append(self.canvas.create_line(self.width, 0, self.width, self.height, width=t, fill=colour))
        #bottom
        self.borderLines.append(self.canvas.create_line(0, self.height, self.width, self.height, width=t, fill=colour))
        #left
        self.borderLines.append(self.canvas.create_line(0, 0, 0, self.height, width=t, fill=colour))
    
    def get_bgColour(self):
        colour = askcolor()

        if colour[0] and colour[1]:  # & &
            bgColour = colour[1]
            self.bgcolourBox['bg'] = colour[1]
        
        self.canvas.configure(background=bgColour)

    def save(self):
        filename = filedialog.asksaveasfilename(title="Select file", filetypes=(("PNG file (.png)", "*.png"), ("all files", "*.*")))
        if filename:
            x = root.winfo_rootx() + self.canvas.winfo_x() + 4
            y = root.winfo_rooty() + self.canvas.winfo_y() + 4
            x1 = x + self.canvas.winfo_width() - 8
            y1 = y + self.canvas.winfo_height() - 8
            ImageGrab.grab().crop((x,y,x1,y1)).save(filename)
            #cv2.imwrite(filename, self.final_image)

    

    def __init__(self, root):
        self.root = root
        self.searchCounter = 2

        tk.Frame.__init__(self, root)

        searchLabel = tk.Label(root, text="Enter search terms:", width=20)
        # searchLabel.pack()
        searchLabel.grid(row=0, column=0, columnspan=2)

        searchEntry = tk.Entry(root)
        # searchEntry.pack()
        searchEntry.grid(row=1, column=0, columnspan=2)

        self.searchBoxes.append(searchEntry)

        self.addButton = tk.Button(root, text="Add", command=self.addLabel)
        # self.addButton.pack()
        self.addButton.grid(row=2, column=0)

        self.removeButton = tk.Button(root, text="Remove", command=self.removeLabel)
        # self.removeButton.pack()
        self.removeButton.grid(row=2, column=1)

        self.searchButton = tk.Button(root, text="Search", command=self.search, width=10)
        # self.searchButton.pack()
        self.searchButton.grid(row=1, column=3)

        self.reArrButton = tk.Button(root, text="Rearrange", command=self.rearrange, width=10)
        # self.reArrButton.pack()
        self.reArrButton.grid(row=2, column=3)

        self.var_bg = tk.IntVar()
        self.chk = tk.Checkbutton(root, text='Background', variable=self.var_bg)
        self.chk.grid(row=1, column=2)

        borderLabel = tk.Label(root, text="Select border:", width=20)
        # borderLabel.pack()
        borderLabel.grid(row=4, column=2)

        self.border = tk.StringVar(root)
        self.border.set("None")
        self.border.trace("w", self.borderSelected)

        self.borderDropdown = tk.OptionMenu(root, self.border, "None", "Solid")
        # self.borderDropdown.pack()
        self.borderDropdown.grid(row=5, column=2)

        self.colourBox = tk.Frame(root, width=50, height=50)
        # self.colourBox.pack()
        self.colourBox.grid(row=6, column=2, rowspan=3)
        self.colourBox['bd'] = 2
        self.colourBox['relief'] = 'ridge'
        self.colourBox.grid_remove()

        self.colourPickerButton = tk.Button(text="Select colour", command=self.getColour)
        # self.colourPickerButton.pack()
        self.colourPickerButton.grid(row=9, column=2)
        self.colourPickerButton.grid_remove()

        self.bgcolourBox = tk.Frame(root, width=50, height=20)
        # self.colourBox.pack()
        self.bgcolourBox.grid(row=1, column=2, rowspan=3)
        self.bgcolourBox['bd'] = 2
        self.bgcolourBox['relief'] = 'ridge'
        #self.bgcolourBox.grid_remove()

        self.bgcolourPickerButton = tk.Button(text="Select colour", command=self.get_bgColour)
        # self.colourPickerButton.pack()
        self.bgcolourPickerButton.grid(row=3, column=2)
        #self.bgcolourPickerButton.grid_remove()

        self.width = 720
        self.height = 720
        self.canvas = tk.Canvas(root, width=self.width, height=self.height)
        self.canvas.grid(row=0, rowspan=100, column=4, columnspan=100)
        self.canvas['bd'] = 2
        self.canvas['relief'] = 'ridge'

        self.refButton = tk.Button(root, text="Refresh", command=self.refresh, width=10)
        # self.searchButton.pack()
        self.refButton.grid(row=3, column=3)

        self.saveButton = tk.Button(root, text="Save", command=self.save, width=10)
        # self.searchButton.pack()
        self.saveButton.grid(row=4, column=3)
        # # checkbox example
        # self.var_gs = tk.IntVar()
        # self.chk = tk.Checkbutton(root, text='Grayscale', variable=self.var_gs)
        # self.chk.grid(row=0, column=2)

        self.label_thickness = tk.Label(root, text="Border thickness")
        self.label_thickness.grid(row=11, column=2)
        self.var_thickness = tk.Scale(root, from_=0, to=200, orient='horizontal', command=self.drawBorder)
        self.var_thickness.grid(row=13, column=2)


root = tk.Tk()
root.wm_title("Collage Maker v0.1")
CollageMaker(root).grid()
root.mainloop()
